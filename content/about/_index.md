+++
draft= false
title = "About"
description = "About"
+++

**Sarah Kastrau**  
is a student of photography at the University of Applied Sciences and Arts in Dortmund. Influenced by the gritty photocopy aesthetics of punk as well as the works of the Japanese Provoke-era photographers, she works with analogue cameras and found footage. Her approach is subversive, biographical and raw, with a penchant for taking an oppositional stance. She is also part of destruktiva, a collective hosting shows for experimental art & music.

**Education**  
since 2015 Student of Photography, University of Applied Sciences and Arts, Dortmund, Germany  
2007 - 2014 Student of Applied Computer Science, Ruhr Universität Bochum, Germany  

**Grants &amp; Awards**  
2019 Canon Student Programme (shortlist)  
2017 Belfast Photo Festival (shortlist)  

**Publications**  
2020 [Concrete Island](/work/concrete/), self-published, Germany      
2020 The Rapid Publisher #15, Germany   
2018 [Fear Of The Monolith](/work/monolith/), self-published, Germany  
2017 [Nein](/work/nein/), /w Flint Stelter, Germany  
2017 [Zum Metzger muss man geboren sein](/work/metzger/), self-published, Germany  
2016 [Goor](/work/goor/), self-published, Germany  
2015 [Life In A Hole](/work/hole/), self-published, Germany  
2015 Landscape, Otto Publishing, Italy  
2015 Grey Magazine #15, Greece  
2014 [Phenomena](/work/phenomena/), self-published, Germany  
2014 [Like Ghosts](/work/ghosts/), self-published, Germany  
2013 dienacht #14, dienacht Publishing, Germany  
2013 [Blinded By The Dark](/work/blinded/), self-published, Germany

**Exhibitions (solo)**  
2019 Concrete Island, secret location, Dortmund/Bochum, Germany  
2018 Lightsick, Umschlagplatz, Dortmund, Germany  

**Exhibitions (group)**  
2019 Fotobus Slideshow at phoszene, Köln, Germany  
2019 vom blättern und wischen, Künstlerhaus, Dortmund, Germany  
2019 Fotobus at World Press Photo, Amsterdam, The Netherlands  
2019 Podest, FH Dortmund, Germany  
2018 destruktiva xi, AZ, Mülheim an der Ruhr, Germany  
2018 Internationaler Kongress zu Fragen der künstlerischen Mobilität, Fotofabrique, Hamburg, Germany  
2018 Podest, FH Dortmund, Dortmund, Germany  
2018 [Lightsick](/work/lightsick/), Infinite Space, Dortmund, Germany  
2017 Psychogeographic Event, Arnhem, Netherlands  
2017 destruktiva, AZ, Mülheim an der Ruhr, Germany  
2016 destruktiva, AZ, Mülheim an der Ruhr, Germany  
2016 Zines Of The World, TIFF Festival, Wroclaw, Poland  
2015 Zines Of The World, Doomed Gallery, London, UK  
2014 Phenomena, CinePalace, Kortrijk, Belgium  
2013 Zines Of The Zone, on the road in Europe  
2013 55m², AZ, Mülheim an der Ruhr, Germany  
2013 untitled, AKZ, Recklinghausen, Germany  

**Collections**  
The Virtual Bookshelf, Austria  
Beinecke Rare Book and Manuscript Library, Yale University, USA  
The Library Project, Ireland<br>Zines of The World, Great Britain  
The Indie Photobook Library, Washington, USA  
Zines Of The Zone, on the road


