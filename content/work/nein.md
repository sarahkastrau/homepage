+++
draft = false
weight = 30
description = "Newspaper, 2017"
title = "Nein"
toc = true
+++

# Nein, 2017
50 pages, newspaper print   
Edition of 20   

<i>My favorite word is „NEIN“. „NEIN“ is liberating. „NEIN“ helps you getting shit done. „NEIN“  frees up time and energy to spend with things or people you love. In the  mid-nineties, I joined a weird cult, mostly made up of people who say  „NEIN“ quite a lot. This is their story.”</i>     

Newspaper photobook by [Flint Stelter](https://flintstelter.com/), who raided his archive of 90ies straight edge hardcore photos and to which I contributed some from my own archive. The world is small and somehow we both hung around in the same scene and the same venues, albeit with a few years difference, only to end up in the same photography class. The pages shown are the ones that contain my own photos (except for the cover). To view the whole project, go to <a href="https://www.flintstelter.com/collection/nein">http://flintstelter.com</a>.</p>


</i>  

![Nein](/img/nein/nein01.jpg)  

![Nein](/img/nein/nein02.jpg)  

![Nein](/img/nein/nein03.jpg)  

![Nein](/img/nein/nein04.jpg)  
