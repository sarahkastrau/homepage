+++
date = "2014"
title = "Phenomena"
draft = false
weight = 80
description = "Zine, 2014"
toc = true
+++

# Phenomena, 2014
24 pages, digital print, 15x15cm  
Edition of 50, self-published  
SOLD OUT  

Made to accompany the same-titled event at CinePalace, Kortrijk, Belgium  

![Phenomena](/img/phenomena/phenomena01.jpg)  

![Phenomena](/img/phenomena/phenomena02.jpg)  

![Phenomena](/img/phenomena/phenomena03.jpg)  

![Phenomena](/img/phenomena/phenomena04.jpg)  

![Phenomena](/img/phenomena/phenomena05.jpg)
