+++
date = "2015"
title = "Life In A Hole"
draft = false
weight = 60
description = "Zine, 2015"
+++

# Life In A Hole, 2015
28 pages, digital print, 15x20cm   
Edition of 80  
Self-published, buy [here](http://sarahkastrau.bigcartel.com)  

<i>"Another year spent in crummy clubs, squatted houses and poorly lit basements. Slowly drifting away from the routines I used to follow. Those on stage seem just as lost as I feel. While everything falls apart, the camera remains my last resort and safe place. It's a life in a hole, and I have yet to reach the bottom."</i>  

![Life In A Hole](/img/hole/hole01.jpg)  

![Life In A Hole](/img/hole/hole02.jpg)  

![Life In A Hole](/img/hole/hole03.jpg)  

![Life In A Hole](/img/hole/hole04.jpg)  

![Life In A Hole](/img/hole/hole05.jpg)  

![Life In A Hole](/img/hole/hole06.jpg)  

![Life In A Hole](/img/hole/hole07.jpg)  

![Life In A Hole](/img/hole/hole08.jpg)  

