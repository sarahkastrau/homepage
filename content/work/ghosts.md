+++
date = "2014"
weight = 70
description = "Zine, 2014"
title = "Like Ghosts"
draft = false
toc = true
+++

# Like Ghosts, 2014  
36 pages, digital print, 15x20cm  
Edition of 60, self-published  
SOLD OUT  

In the pre-digital age the paper negatives that came with peel-apart instant film used to be nothing but trash. While being necessary for processing the image, their opaque flipside made it impossible to put them in an enlarger like a regular film negative. What was left after glancing at the modern miracle of instant film were thin white sheets of paper with no further use. Like ghosts they are of fragile nature, sensitive to light and their lifespan is limited since oxydation of leftover chemicals will destroy the paper over time.  

![Like Ghosts](/img/ghosts/ghosts01.jpg)  

![Like Ghosts](/img/ghosts/ghosts02.jpg)  

![Like Ghosts](/img/ghosts/ghosts03.jpg)  

![Like Ghosts](/img/ghosts/ghosts04.jpg)  

![Like Ghosts](/img/ghosts/ghosts06.jpg)  

![Like Ghosts](/img/ghosts/ghosts07.jpg)  

![Like Ghosts](/img/ghosts/ghosts08.jpg)  

![Like Ghosts](/img/ghosts/ghosts09.jpg)  
