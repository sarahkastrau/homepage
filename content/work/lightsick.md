+++
title = "Lightsick"
description = "Exhibition, 2018"
weight = 10
draft = false
toc = true
bref = ""
+++

# Lightsick, 2018/ongoing

„Lightsick“ is a re-visitation of the leftist, self-organized venues in which I spend most of my weekends since I was sixteen. These places introduced me to a whole host of concepts and ideas such as d.i.y., animal rights and gender equality which shaped my personal world view and which I still value. The like-minded people I found here also supported me through the first years of my photographic journey. This support eventually helped me to gain the confidence to quit studying Applied Computer Science, a subject that, while deeply interesting, ultimately resulted in depression and a burnout for me. 

This series is my way of paying tribute to those squatted houses, basement rehearsal spaces and illegal clubs. They are more to me than mere physical containers, they are mind-spaces as well. A refuge from a world where experiences are increasingly commodified and streamlined, in order to be easily monetized. They proceed to offer a safe space for self-expression for those who can not or will not fit into the mainstream.

![Lightsick](/img/lightsick/lightsick01.jpg)  

![Lightsick](/img/lightsick/lightsick02.jpg)  

![Lightsick](/img/lightsick/lightsick03.jpg)  

![Lightsick](/img/lightsick/lightsick04.jpg)  

![Lightsick](/img/lightsick/lightsick05.jpg)  

![Lightsick](/img/lightsick/lightsick06.jpg)  

![Lightsick](/img/lightsick/lightsick07.jpg)  

![Lightsick](/img/lightsick/lightsick08.jpg)  

![Lightsick](/img/lightsick/lightsick09.jpg)  

![Lightsick](/img/lightsick/lightsick10.jpg)  

Exhibition photos ©[Leopold Achilles](https://achillesphotos.com/)