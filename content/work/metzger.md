+++
title = "Zum Metzger muss man geboren sein"
weight = 25
draft = false
description = "Book, 2018"
toc = true
+++

# Zum Metzger muss man geboren sein, 2017
52 pages, digital print, 20x24 cm  
Edition of 8, self-published     
SOLD OUT

„He was such a nice guy.“   
„ I never thought something so awful would happen in our town.“  

These are the typical quotes we hear whenever a serious crime is shown on the news. We all know there are bad people out there, we just don‘t expect them to live next door. It is a disturbing feeling to realize one has been fooled by a friendly facade. The betrayal however, is always twofold. For first, someone has betrayed our trust, lied to us. But what‘s worse, our mind has betrayed us as well. We rely on our ability to tell truth from lie, for we depend on it to navigate our world. But how can we trust others, when we can‘t even trust our own judgement?  

„Zum Metzger muss man geboren sein“ tries to reconstruct an unresolved crime case which took place during the German Wirtschaftwunder-era of the 60ies. Compiled from found footage, vintage photographs and old newspaper articles, it is an exploration of the slow deterioation of perceived truth, trying to recreate that paranoid feeling when one realizes things are not what they seemed to be.  

*Shortlisted for the Dummy Award at Belfast Photo Festival 2017*

![Zum Metzger muss man geboren sein](/img/metzger/metzger01.jpg)  

![Zum Metzger muss man geboren sein](/img/metzger/metzger02.jpg)  

![Zum Metzger muss man geboren sein](/img/metzger/metzger03.jpg)  

![Zum Metzger muss man geboren sein](/img/metzger/metzger04.jpg)  

![Zum Metzger muss man geboren sein](/img/metzger/metzger05.jpg)  

![Zum Metzger muss man geboren sein](/img/metzger/metzger06.jpg)  

![Zum Metzger muss man geboren sein](/img/metzger/metzger07.jpg)   

![Zum Metzger muss man geboren sein](/img/metzger/metzger08.jpg)  

![Zum Metzger muss man geboren sein](/img/metzger/metzger09.jpg)


