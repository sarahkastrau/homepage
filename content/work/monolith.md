+++
title = "Fear Of The Monolith"
description = "Book, 2018"
weight = 20
draft = false
toc = true
bref = ""
+++

# Fear Of The Monolith, 2018
36 pages, digital print, 13x18,5cm   
Open edition  
Self-published, buy [here](http://sarahkastrau.bigcartel.com)  



Tracing the connections between brutalism and the alien architecture of H.P. Lovecraft, using found footage, strange maps and blurry childhood memories.

While the original idea of the monolith as something more than being a mere slab of stone can be attributed to "2001: A Space Odysee", the word itself has snuck it's way into fiction before, and subsequently made regular apperarance in the description of actually existing architecture. And whenever it is used, it always seems to describe something that is somehow opposed to human existence. Indifferent at best, hostile at worst, often leaving a feeling of incomprehensibly. This zine is the first attempt to grasp what I consider to be a powerful symbol of our fear of the unkown.

![Fear Of The Monolith](/img/monolith/monolith01.jpg)  

![Fear Of The Monolith](/img/monolith/monolith02.jpg)  

![Fear Of The Monolith](/img/monolith/monolith03.jpg)  

![Fear Of The Monolith](/img/monolith/monolith04.jpg)  

![Fear Of The Monolith](/img/monolith/monolith05.jpg)  

![Fear Of The Monolith](/img/monolith/monolith06.jpg)  

![Fear Of The Monolith](/img/monolith/monolith07.jpg)