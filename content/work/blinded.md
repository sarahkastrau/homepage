+++
date = "2013"
title = "Blinded By The Dark"
draft = false
weight = 80
description = "Zine, 2013"
toc = true
+++

# Blinded By The Dark, 2013
28 pages, digital print, 15x20cm   
Edition of 50, signed & numbered  
Self-published, SOLD OUT  

<i>"What My Bloody Valentine do with reverb she does with her camera: create a wide, open room filled with blurry atmosphere."</i>  

![Blinded By The Dark](/img/blinded/blinded01.jpg)

![Blinded By The Dark](/img/blinded/blinded02.jpg)

![Blinded By The Dark](/img/blinded/blinded03.jpg)

![Blinded By The Dark](/img/blinded/blinded04.jpg)

![Blinded By The Dark](/img/blinded/blinded05.jpg)

![Blinded By The Dark](/img/blinded/blinded06.jpg)

![Blinded By The Dark](/img/blinded/blinded07.jpg)

![Blinded By The Dark](/img/blinded/blinded08.jpg)

![Blinded By The Dark](/img/blinded/blinded09.jpg)
