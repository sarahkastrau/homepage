+++
date = "2016"
title = "Goor"
draft = false
weight = 50
description = "Zine, 2016"
toc = true
+++

# Goor, 2016
24 pages, digital print, 15x20cm   
Edition of 10, self-published   
SOLD OUT

*„It started back in 2010 with Dirk Serries’ project Microphonics. I had met Dirk Serries
a couple of times before, and he told me he had done some shows in living rooms.
At that point, I thought “Wow, how cool is that, having an artist playing at your home!
Could I do something like this?” The idea somewhat moved to the back of my mind
until the next time I met Dirk at one of his shows. I decided to ask him if he would be
interested in doing a show in Goor! And much to my surprise he said “sure, if we can
fit it into the upcoming tour!” That was almost 5 years ago...“*   
[livingroommusic.wordpress.com](https://livingroommusic.wordpress.com/)  

![Goor](/img/goor/goor01.jpg)  

![Goor](/img/goor/goor02.jpg)

![Goor](/img/goor/goor03.jpg)

![Goor](/img/goor/goor04.jpg)

![Goor](/img/goor/goor05.jpg)

![Goor](/img/goor/goor06.jpg)

![Goor](/img/goor/goor07.jpg)

![Goor](/img/goor/goor08.jpg)