+++
description = "Sarah Kastrau, Photographer"
title = "Photography"
draft = false
+++

Hi, I'm [Sarah Kastrau](/about/), a student of [Photography](/work/). Right now I'm working on [Concrete Island](https://concrete-island.com), an interactive exhibition in public space. Follow along on [Instagram](https://instagram.com/sarahkastrau) for updates on the project. Got questions? Feel free to [contact](/contact/) me.
